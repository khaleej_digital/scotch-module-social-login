<?php
/**
 * Copyright © Redbox Digital, Inc. All rights reserved.
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Beside_SocialLogin', __DIR__);
